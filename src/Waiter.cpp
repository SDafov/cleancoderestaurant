#include "pch.h"
#include "Employee.h"
#include "Waiter.h"

#include <iostream>
#include <cassert>

const bool DEFAULT_CAN_SERVICE_VIP = false;

Waiter::Waiter() : Employee()
{
	canServiceVIP = DEFAULT_CAN_SERVICE_VIP;
}

Waiter::Waiter(std::string name, std::string addressByID, int age, int jobsInTheFieldBefore, int monthsInService,
	double experienceRate, bool canServiceVIP) : Employee(name, addressByID, age, jobsInTheFieldBefore, monthsInService,
		experienceRate)
{
	this->canServiceVIP = canServiceVIP;
}

Waiter::~Waiter()
{}

Waiter::Waiter(const Waiter &otherWaiter) : Employee(otherWaiter)
{
	canServiceVIP = otherWaiter.canServiceVIP;
}

Waiter& Waiter::operator=(const Waiter &otherWaiter)
{
	if (this != &otherWaiter)
	{
		Employee::operator=(otherWaiter);
		canServiceVIP = otherWaiter.canServiceVIP;
	}

	return *this;
}

bool Waiter::getServiceQualification() const
{
	return canServiceVIP;
}

void Waiter::setServiceQualification(bool canServiceVIP)
{
	this->canServiceVIP = canServiceVIP;
}