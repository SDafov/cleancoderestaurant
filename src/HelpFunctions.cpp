#include "pch.h"
#include "HelpFunctions.h"

#include <iostream>
#include <cstring>

Table createTable()
{
	int numberOfSeats = 320;
	double distanceToLoo = 5.6;
	double distanceToExit = 12.2;
	double distanceToPlayground = 4.2;
	std::string reservedHours = "11.00, 12.30, 16.30, 21.00";
	TABLE_STATUS status = RESERVED;

	Table table(numberOfSeats, distanceToLoo, distanceToExit, distanceToPlayground, reservedHours, status);

	return table;
}

Employee createEmployee()
{
	std::string name = "Ron";
	std::string addressByID = "Some street 19";

	int age = 28;
	int jobsInTheFieldBefore = 2;
	int monthsInService = 23;
	double experienceRate = 3.4;

	Employee employee(name, addressByID, age, jobsInTheFieldBefore, monthsInService, experienceRate);

	return employee;
}

std::vector<Table> createTables()
{
	std::vector<Table> tables;

	Table firstTable(4, 2, 2, 2, "NONE", NON_RESERVED);
	Table secondTable = createTable();

	tables.push_back(firstTable);
	tables.push_back(secondTable);

	return tables;
}

std::vector<Employee> createEmployees()
{
	std::vector<Employee> employees;

	Employee firstEmployee("Judd", "Some street 18", 22, 0, 12, 2.1);
	Employee secondEmployee = createEmployee();

	employees.push_back(firstEmployee);
	employees.push_back(secondEmployee);

	return employees;
}

Client createClient()
{
	std::string name = "John";
	bool isVIPmember = true;
	int previousVisits = 262;
	double moneySpentSoFar = 101;

	Client client(name, isVIPmember, previousVisits, moneySpentSoFar);

	return client;
}