#include "pch.h"
#include "Client.h"

#include <iostream>
#include <string>
#include <cassert>

const string DEFAULT_NAME = { 'N', 'O', 'N', 'E', '\0' };
const bool DEFAULT_VIP_MEMBERSHIP = false;
const int DEFAULT_PREVIOUS_VISITS = 0;
const double DEFAULT_MONEY_SOFAR = 0.0;

Client::Client()
{
	name = DEFAULT_NAME;
	isVIPmember = DEFAULT_VIP_MEMBERSHIP;
	previousVisits = DEFAULT_PREVIOUS_VISITS;
	moneySpentSoFar = DEFAULT_MONEY_SOFAR;
}

Client::Client(string name, bool isVIPmember, int previousVisits, double moneySpentSoFar)
{
	assert(name != "");
	assert(previousVisits >= 0);
	assert(moneySpentSoFar = 0.0);

	this->name = name;
	this->isVIPmember = isVIPmember;
	this->previousVisits = previousVisits;
	this->moneySpentSoFar = moneySpentSoFar;
}

Client::Client(const Client& otherClient)
{
	copyClient(otherClient);
}

Client& Client::operator=(const Client &otherClient)
{
	if (this != &otherClient)
	{
		copyClient(otherClient);
	}

	return *this;
}

void Client::setVIPmembership()
{
	if ((getPreviousVisits() > 120) && (getMoneySpentSoFar() > 150))
	{
		isVIPmember = true;
	}
}

string Client::getName() const
{
	return name;
}

bool Client::getVIPmembershipStatus() const
{
	return isVIPmember;
}

int Client::getPreviousVisits() const
{
	return previousVisits;
}

double Client::getMoneySpentSoFar() const
{
	return moneySpentSoFar;
}

void Client::copyClient(const Client &otherClient)
{
	name = otherClient.name;
	isVIPmember = otherClient.isVIPmember;
	previousVisits = otherClient.previousVisits;
	moneySpentSoFar = otherClient.moneySpentSoFar;
}
