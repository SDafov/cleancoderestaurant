#include "pch.h"
#include "Table.h"

#include <cassert>
#include <iostream>

const int DEFAULT_NUMBER_OF_SEATS = 0;
const double DEFAULT_DISTANCE_TO_LOO = 0;
const double DEFAULT_DISTANCE_TO_EXIT = 0;
const double DEFAULT_DISTANCE_TO_PLAYGROUND = 0;
const std::string DEFAULT_RESERVED_HOURS = { 'N', 'O', 'N', 'E', '\0' };
const TABLE_STATUS DEFAULT_STATUS = NON_RESERVED;


Table::Table()
{
	numberOfSeats = DEFAULT_NUMBER_OF_SEATS;
	distanceToLoo = DEFAULT_DISTANCE_TO_LOO;
	distanceToExit = DEFAULT_DISTANCE_TO_EXIT;
	distanceToPlayground = DEFAULT_DISTANCE_TO_PLAYGROUND;
	reservedHours = DEFAULT_RESERVED_HOURS;
	status = DEFAULT_STATUS;
}

Table::Table(int numberOfSeats, double distanceToLoo, double distanceToExit, double distanceToPlayground,
	std::string reservedHours, TABLE_STATUS status)
{
	assert(numberOfSeats >= 0);
	assert(distanceToLoo >= 0);
	assert(distanceToExit >= 0);
	assert(distanceToPlayground >= 0);
	assert(reservedHours.length() >= 0);

	this->numberOfSeats = numberOfSeats;
	this->distanceToLoo = distanceToLoo;
	this->distanceToExit = distanceToExit;
	this->distanceToPlayground = distanceToPlayground;
	this->status = status;
}

Table::~Table()
{}

Table::Table(const Table &otherTable)
{
	copyTable(otherTable);
}

Table& Table::operator=(const Table &otherTable)
{
	if (this != &otherTable)
	{
		copyTable(otherTable);
	}
	
	return *this;
}

void Table::copyTable(const Table &otherTable)
{
	numberOfSeats = otherTable.numberOfSeats;
	distanceToLoo = otherTable.distanceToLoo;
	distanceToExit = otherTable.distanceToExit;
	distanceToPlayground = otherTable.distanceToPlayground;
	reservedHours = otherTable.reservedHours;
	status = otherTable.status;
}


int Table::getNumberOfSeats() const
{
	return numberOfSeats;
}

double Table::getDistanceToLoo() const
{
	return distanceToLoo;
}

double Table::getDistanceToExit() const
{
	return distanceToExit;
}

double Table::getDistanceToPlayground() const
{
	return distanceToPlayground;
}

std::string Table::getReservedHours() const
{
	return reservedHours;
}

TABLE_STATUS Table::getStatus() const
{
	return status;
}

bool Table::isSuitableForKids() const
{
	if (distanceToPlayground <= 18)
	{
		return true;
	}

	return false;
}

bool Table::isSuitableForDisabled() const
{
	if (((distanceToExit + distanceToLoo) / 2) <= 7.5)
	{
		return true;
	}

	return false;
}

