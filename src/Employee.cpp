#include "pch.h"
#include "Employee.h"

#include <iostream>
#include <cstring>
#include <cassert>

const std::string DEFAULT_NAME = { 'N', 'A', 'M', 'E', '\0' };
const std::string DEFAULT_ADDRESS = { 'A', 'D', 'D', 'R', 'E', 'S', 'S', '\0' };
const int DEFAULT_AGE = 0;
const int DEFAULT_JOBS_BEFORE = 0;
const int DEFAULT_MONTHS_IN_SERVICE = 0;
const double DEFAULT_EXPERIENCE_RATE = 0;
const double DEFAULT_SALARY = 0.0;

Employee::Employee()
{
	name = DEFAULT_NAME;
	addressByID = DEFAULT_ADDRESS;
	age = DEFAULT_AGE;
	jobsInTheFieldBefore = DEFAULT_JOBS_BEFORE;
	monthsInService = DEFAULT_MONTHS_IN_SERVICE;
	experienceRate = DEFAULT_EXPERIENCE_RATE;
	salary = DEFAULT_SALARY;
}

Employee::Employee(std::string name, std::string addressByID, int age, int jobsInTheFieldBefore,
	int monthsInService, double experienceRate)
{
	assert(name.length() > 0);
	assert(addressByID.length() > 0);
	assert(age > 18);
	assert(jobsInTheFieldBefore >= 0);
	assert(monthsInService >= 0);
	assert(experienceRate > 0);

	this->age = age;
	this->jobsInTheFieldBefore = jobsInTheFieldBefore;
	this->monthsInService = monthsInService;
	this->experienceRate = experienceRate;
}

Employee::~Employee()
{}

Employee::Employee(const Employee &otherEmployee)
{
	copyEmployee(otherEmployee);
}

Employee& Employee::operator=(const Employee &otherEmployee)
{
	if (this != &otherEmployee)
	{
		copyEmployee(otherEmployee);
	}

	return *this;
}

std::string Employee::getName() const
{
	return name;
}

std::string Employee::getAddress() const
{
	return addressByID;
}

int Employee::getAge() const
{
	return age;
}

int Employee::getJobsInTheFieldBefore() const
{
	return jobsInTheFieldBefore;
}

int Employee::getMonthsInService() const
{
	return monthsInService;
}

double Employee::getExperienceRate() const
{
	return experienceRate;
}

void Employee::copyEmployee(const Employee &otherEmployee)
{
	name = otherEmployee.name;
	addressByID = otherEmployee.addressByID;
	age = otherEmployee.age;
	jobsInTheFieldBefore = otherEmployee.jobsInTheFieldBefore;
	monthsInService = otherEmployee.monthsInService;
	experienceRate = otherEmployee.experienceRate;
}


double Employee::calculateExperienceRate() const
{
	return getJobsInTheFieldBefore()*getMonthsInService() / 4;
}