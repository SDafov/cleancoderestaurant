#include "pch.h"
#include "Restaurant.h"

#include <iostream>
#include <cassert>
#include <cstring>

const std::string DEFAULT_NAME = { 'N', 'A', 'M', 'E', '\0' };
const std::string DEFAULT_ADDRESS = { 'N', 'O', 'N', 'E', '\0' };
const int DEFAULT_FOUNDATION_YEAR = 0;
const int DEFAULT_NUMBER_OF_EMPLOYEES = 0;
const int DEFAULT_NUMBER_OF_TABLES = 0;
const int DEFAULT_NUMBER_OF_SEATS = 0;
const double DEFAULT_PRESTIGE_RATE = 0.0;


Restaurant::Restaurant()
{
	name = DEFAULT_NAME;
	address = DEFAULT_ADDRESS;
	foundationYear = DEFAULT_FOUNDATION_YEAR;
	prestigeRate = DEFAULT_PRESTIGE_RATE;
}

Restaurant::Restaurant(std::string name,std::string address, int foundationYear, double prestigeRate,
	std::vector<Table> tables, std::vector<Employee> employees)
{
	assert(name.length() > 0);
	assert(address.length() > 0);
	assert(foundationYear > 0);
	assert(tables.size() >= 0);
	assert(employees.size() >= 0);

	this->name = name;
	this->address = address;
	this->foundationYear = foundationYear;
	this->tables = tables;
	this->employees = employees;
}

Restaurant::Restaurant(const Restaurant &otherRestaurant)
{
	copyRestaurant(otherRestaurant);
}

Restaurant::~Restaurant()
{}

Restaurant& Restaurant::operator=(const Restaurant &otherRestaurant)
{
	if (this != &otherRestaurant)
	{
		copyRestaurant(otherRestaurant);
	}

	return *this;
}

std::string Restaurant::getName() const
{
	return this->name;
}

std::string Restaurant::getAddress() const
{
	return this->address;
}

int Restaurant::getFoundationYear() const
{
	return this->foundationYear;
}

double Restaurant::getPrestigeRate() const
{
	return this->prestigeRate;
}

int Restaurant::getNumbersOfEmployees() const
{
	return employees.size();
}

int Restaurant::getNumberOfTables() const
{
	return tables.size();
}

void Restaurant::copyRestaurant(const Restaurant &otherRestaurant)
{
	name = otherRestaurant.name;
	address = otherRestaurant.address;
	foundationYear = otherRestaurant.foundationYear;
	prestigeRate = otherRestaurant.prestigeRate;
	tables = otherRestaurant.tables;
	employees = otherRestaurant.employees;
}