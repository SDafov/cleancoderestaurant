#include "pch.h"
#include "Chef.h"

#include <iostream>

Chef::Chef() : Employee()
{
	title = COMMIS_CHEF;
}

Chef::Chef(const char* name, const char* addressByID, int age, int jobsInTheFieldBefore,
	int monthsInService, double experienceRate, CHEF_TITLE title) : Employee(name, addressByID, age, jobsInTheFieldBefore, monthsInService,
		experienceRate)
{
	title = COMMIS_CHEF;
}

Chef::~Chef()
{}

Chef::Chef(const Chef &otherChef) : Employee(otherChef)
{
	title = otherChef.title;
}

Chef& Chef::operator=(const Chef &otherChef)
{
	if (this != &otherChef)
	{
		Employee::operator=(otherChef);
		title = otherChef.title;
	}

	return *this;
}

CHEF_TITLE Chef::getHierarchyPlace() const
{
	return title;
}

void Chef::setHierarchyPlace(const CHEF_TITLE &someTitle)
{
	title = someTitle;
}