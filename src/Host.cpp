#include "pch.h"
#include "Host.h"

#include <iostream>

const bool DEFAULT_IS_QUALIFIED_FOR_WINETASTING = false;

Host::Host() : Employee()
{
	isQualifiedToTasteWine = DEFAULT_IS_QUALIFIED_FOR_WINETASTING;
}

Host::Host(const char* name, const char* addressByID, int age, int jobsInTheFieldBefore,
	int monthsInService, double experienceRate, bool isQualifiedToTasteWine) : Employee(name, addressByID, age, jobsInTheFieldBefore, 
		monthsInService, experienceRate)
{
	this->isQualifiedToTasteWine = isQualifiedToTasteWine;
}

Host::~Host()
{}

Host::Host(const Host& otherHost) : Employee(otherHost)
{
	isQualifiedToTasteWine = otherHost.isQualifiedToTasteWine;
}

Host& Host::operator=(const Host& otherHost)
{
	if (this != &otherHost)
	{
		Employee::operator=(otherHost);
		isQualifiedToTasteWine = otherHost.isQualifiedToTasteWine;
	}

	return *this;
}

bool Host::getQualificationForWinetasting() const
{
	return isQualifiedToTasteWine;
}

void Host::setWinetastingQualification(bool qualificationStatus)
{
	isQualifiedToTasteWine = qualificationStatus;
}