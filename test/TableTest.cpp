#include "pch.h"
#include "gtest/gtest.h"
#include "../Restraurant_clean_code/Table.h"
#include "../Restraurant_clean_code/HelpFunctions.h"

#include <iostream>
#include <cassert>
#include <cstring>
#include <vector>


TEST(TableTest, IsCreatedSuccessfully) 
{
	Table firstTable = createTable();
	Table secondTable = Table(320, 5.6, 12.2, 4.2, "11.00, 12.30, 16.30, 21.00", RESERVED);

    EXPECT_EQ(firstTable.getDistanceToExit(), secondTable.getDistanceToExit());
}

TEST(TableTest, isSuitableForKids_IS_SUCCESSFUL)
{
	Table testTable = createTable();
	bool isSuitableForChildren = testTable.isSuitableForKids();

	EXPECT_TRUE(isSuitableForChildren);
}

TEST(TableTest, canCreateVectorOfTables)
{
	std::vector<Table> tables = createTables();

	EXPECT_EQ(tables.size(), 2);
}