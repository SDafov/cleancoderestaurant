#include "pch.h"
#include "gtest/gtest.h"
#include "../Restraurant_clean_code/Client.h"
#include "../Restraurant_clean_code/HelpFunctions.h"

#include <iostream>
#include <cassert>
#include <cstring>


TEST(ClientTest, isCreatedSuccessfully) 
{
	Client client = createClient();
	bool areTheSame = (client.getName() == "John");

	EXPECT_TRUE(areTheSame);
}