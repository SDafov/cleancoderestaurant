#include "pch.h"
#include "gtest/gtest.h"
#include "../Restraurant_clean_code/Employee.h"
#include "../Restraurant_clean_code/HelpFunctions.h"

#include <iostream>
#include <cassert>
#include <cstring>
#include <vector>


TEST(EmployeeTest, IsCreatedSuccessfully)
{
	Employee firstEmployee = createEmployee();
	Employee secondEmployee = Employee("RON", "Some street 19", 28, 2, 23, 3.4);

	EXPECT_EQ(firstEmployee.getAge(), secondEmployee.getAge());
}

TEST(EmployeeTest, canCreateVectorOfEmployees)
{
	std::vector<Employee> employees = createEmployees();

	EXPECT_EQ(employees.size(), 2);
}