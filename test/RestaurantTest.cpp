#include "pch.h"
#include "gtest/gtest.h"
#include <iostream>
#include <cassert>
#include <cstring>
#include <vector>

#include "../Restraurant_clean_code/Restaurant.h"
#include "../Restraurant_clean_code/HelpFunctions.h"


Restaurant createRestaurant()
{
	std::string name = "Le Bernardin";
	std::string address = "155 W 51st St, New York, NY 10019";
	int foundationYear = 1986;
	double prestigeRate = 9.8; // out of 10
	std::vector<Table> tables = createTables();
	std::vector<Employee> employees = createEmployees();

	Restaurant restaurant(name, address, foundationYear, prestigeRate, tables, employees);
	
	return restaurant;
}

TEST(RestaurantTest, HasCorrectName)
{
	Restaurant restaurant = createRestaurant();
	std::string testName = "Le Bernardin";

	bool areTheSame = (restaurant.getName() == testName);

    EXPECT_TRUE(areTheSame);	
}