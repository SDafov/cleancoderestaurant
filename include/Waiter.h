#pragma once
#ifndef WAITER_H
#define WAITER_H

#include "Employee.h"
#include <string.h>

class Waiter : public Employee
{
public:
	Waiter();
	Waiter(std::string, std::string, int, int, int, double, bool);
	~Waiter();
	Waiter(const Waiter&);
	Waiter& operator=(const Waiter&);

	bool getServiceQualification() const;
	void setServiceQualification(bool); 

private:
	bool canServiceVIP;
};

#endif