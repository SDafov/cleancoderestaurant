#pragma once
#ifndef RESTAURNTBUILDING_H
#define RESTAURANTBULDING_H

#include <iostream>
#include <string.h>
#include <vector>

#include "Employee.h"
#include "Table.h"

class Restaurant 
{
public:
	Restaurant();
	Restaurant(std::string, std::string, int, double, std::vector<Table>, std::vector<Employee>);
	~Restaurant();
	Restaurant(const Restaurant&);
	Restaurant& operator=(const Restaurant&);

	std::string getName() const;
	std::string getAddress() const;

	int getFoundationYear() const;
	int getNumbersOfEmployees() const;
	int getNumberOfTables() const;             

	double getPrestigeRate() const;

private:
	std::string name;
	std::string address;
	int foundationYear;
	double prestigeRate;
	std::vector<Table> tables;
	std::vector<Employee> employees;

	void copyRestaurant(const Restaurant&);
};

#endif