#pragma once

#include <cstring>

#include "Restaurant.h"
#include "Employee.h"
#include "Chef.h"
#include "Table.h"
#include "Waiter.h"
#include "Client.h"
#include "Host.h"

Table createTable();
std::vector<Table> createTables();

Employee createEmployee();
std::vector<Employee> createEmployees();

Client createClient();