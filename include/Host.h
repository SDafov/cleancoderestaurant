#pragma once
#ifndef HOST_H
#define HOST_H

#include "Employee.h"

class Host : public Employee
{
public:
	Host();
	Host(const char*, const char*, int, int, int, double, bool);
	~Host();
	Host(const Host&);
	Host& operator=(const Host&);

	bool getQualificationForWinetasting() const;
	void setWinetastingQualification(bool);

private:
	bool isQualifiedToTasteWine;
};

#endif // !HOST_H
