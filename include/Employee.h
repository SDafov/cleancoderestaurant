#pragma once
#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>
#include <string.h>

class Employee 
{
public:
	Employee();
	Employee(std::string, std::string, int, int, int, double);
	virtual ~Employee();
	Employee(const Employee&);
	Employee& operator=(const Employee&);

	std::string getName() const;
	std::string getAddress() const;

	int getAge() const;
	int getJobsInTheFieldBefore() const;  
	int getMonthsInService() const;
	double getExperienceRate() const;

protected:
	double salary;

private:
	std::string name;
	std::string addressByID;

	int age;
	int jobsInTheFieldBefore;
	int monthsInService;
	double experienceRate;

	double calculateExperienceRate() const;

	void copyEmployee(const Employee&);
};

#endif // !EMPLOYEE_H
