#pragma once
#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <string.h>

using namespace std;

class Client
{
public:
	Client();
	Client(string, bool, int, double);
	Client(const Client&);
	Client& operator=(const Client&);

	std::string getName() const;
	bool getVIPmembershipStatus() const;
	int getPreviousVisits() const;
	double getMoneySpentSoFar() const;

	void setVIPmembership();

private:
	std::string name;
	bool isVIPmember;
	int previousVisits; 
	double moneySpentSoFar;	 // in thousand dollars;

	void copyClient(const Client&);
};

#endif // !CLIENT_h
