#pragma once
#ifndef TABLE_H
#define TABLE_H

#include <iostream>
#include <string.h>

enum TABLE_STATUS
{
	RESERVED,
	NON_RESERVED
};

class Table
{
public:
	Table();
	Table(int, double, double, double, std::string, TABLE_STATUS);
	~Table();
	Table(const Table&);
	Table& operator=(const Table&);

	int getNumberOfSeats() const;

	double getDistanceToLoo() const;
	double getDistanceToExit() const;
	double getDistanceToPlayground() const;

	std::string getReservedHours() const;

	TABLE_STATUS getStatus() const;

	bool isSuitableForKids() const;
	bool isSuitableForDisabled() const;

private:
	int numberOfSeats;

	double distanceToLoo;
	double distanceToExit;
	double distanceToPlayground;
	
	std::string reservedHours;

	TABLE_STATUS status;

	void copyTable(const Table&);
};

#endif