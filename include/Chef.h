#pragma once
#ifndef CHEF_H
#define CHEF_H

#include "Employee.h"

enum CHEF_TITLE
{
	CHEF_DE_CUISINE,
	SOUS_CHEF,
	CHEF_DE_PARTIE,
	COMMIS_CHEF
};

class Chef : public Employee
{
public:
	Chef();
	Chef(const char*, const char*, int, int, int, double, CHEF_TITLE);
	~Chef();
	Chef(const Chef&);
	Chef& operator=(const Chef&);

	CHEF_TITLE getHierarchyPlace() const;

	void setHierarchyPlace(const CHEF_TITLE&);

private:
	CHEF_TITLE title;
};


#endif // !CHEF_H
